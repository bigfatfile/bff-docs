# Glossary

## Data model

### Access

-   a dictionary which maps member IDs (Users or Teams) to Access Levels;
-   usually provided as the `.access` property on a Model to govern access on the Model's children, e.g:
    -   `Team.access` governs access to the Team's members;
    -   `Drive.access` governs access to the Driver's Nodes;
    -   `Share.access` governs access to the Share's target Nodes;
-   **Member**:
    -   any User or Team (designated by ID);
    -   IDs are unique across all member types;
-   **Permission**:
    -   authorizes a specific operation (e.g. `READ`, `WRITE` etc.);
    -   defined as 1-bit binary numbers (to allow bitwise OR);
-   **Level**:
    -   integer representing a bit-sum of permissions (e.g. `WRITER` level is `READ|WRITE`);

### Account

-   **Description**:
    -   This model can represent _a user_ (i.e. single individual) or _an organization_ (e.g. business etc.); this depends on the internal `type` field (see `AccountType`).
    -   It's an enhanced version of the `Meteor.User` object.
-   **Notes**:
    -   ID syntax: `account_*`
    -   Special IDs:
        -   `account_system`: represents the BFF back-end system;
        -   `account_everyone`: represents any user (even unauthenticated).

### Team

-   a group of users (representing a company's staff or a department);
-   team IDs always start with the `team-` prefix;

### Store

-   a back-end storage configuration;
-   store IDs always start with the `store-` prefix;
-   the internals of this model are hidden from Nodes and Drives;

### Drive / DriveFull

-   a container of Nodes;
-   drive IDs always start with the `drive-` prefix;
-   provides `.access` rules, governing access to contained Nodes;
-   uses a single Store;

### Node / NodeFull

-   a single file or directory residing inside a Drive;
-   node IDs always start with the `node-` prefix;
-   all Nodes obay the Drive's `.access` rules;

### Share

-   a distinct set of `.access` rules to a specific Node (or list of Nodes);
-   share IDs always start with the `share-` prefix;
-   normally access to Nodes is governed by the Driver's `.access` rules;
-   this allows explicit sharing separately from the Drive's rules;
