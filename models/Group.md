# The Group Model

The Group represents a collection of user Accounts and/or other Groups (i.e. nested Groups).
Such a Group can be used to assign Access Levels (permissions) to certain objects such as Drives, etc.

## Properties

-   `_id`: string (pattern: "group\_\*")
-   `type`: GroupType ("regular" | "organization")
-   `ownerId`: string (pattern: "account\_\*")
-   `access`: array of AccessRule objects
-   `memberOf`: array of Membership
-   `name`: string (minLength: 2, maxLength: 256)
-   `description`: string (maxLength: 1024)
-   `createdAt`: string (format: "date-time")
-   `deletedAt`: string (format: "date-time")
-   `_runtime`: object (any)

### Sample Object

```ts
{
    _id: "group_id",
    type: "regular",
    ownerId: "account_id",
    access: [
        { actorId: 'account_id', level: 'manager' }
    ],
    memberOf: [ "group_id" ],
    name: "Example Group",
    description: "This is an example group.",
    createdAt: "2022-01-01T00:00:00Z",
    deletedAt: "2022-01-02T00:00:00Z",
    _runtime: {}
}
```

## Special cases

There is a special type of Group called an "Organization Group" which is created automatically for
every Account which is elevated to an Organization type. This Group is implicit and will automatically
contain all Accounts which are members of the owner Organization.

These Groups have their ID equal to `group_<ownerId>` wher `<ownerId>` is the Group's Owner ID, which
is an Account ID. E.g. `group_account_123...`. This implies that for any Organization Account we can
easily get the ID of the corresponding Organization Group by just prepending `group_` to the same
Account ID.

Removing an Account from this Group will implicitly mean that the Account will be removed from ALL
Groups owned by the associated Organization (since logically this means to remove the Account from the
Organization entirely).

In all other respects this Group will behave the same as any other (regular) Group.
