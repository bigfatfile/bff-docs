# Models

BFF data is internally defined as a hierarchy of **Model** classes, all derived from the `abstract/Model.ts` base class.

Every Model has an `_id` field which is unique across the entire database and always starts with a prefix which is the lowercase Model's name (e.g. `account_123...` or `drive_123...`). This is an intentional decision enforced across the entire BFF ecosystem.

> [!TIP] This means we can always infer the Model type just by looking at the `_id` of any object (e.g. an object with ID `drive_123...` is obviously an instance of the Drive model).

## Validation

Model data is validated using an internal [JSON Schema](https://json-schema.org/) definition. Validation is enforced:

-   anytime the Model is saved to the database;
-   when filled with data using `fillAndValidate()`;
-   on-demand by calling the `validate()` and `validateOrThrow()` methods.

> [!TIP] You can obtain the validation schema for any Model class by simply invoking the `schema()` method on the instance.

## Access control

The BFF authorization system is intentionally very simple and is based around the concept of **"Access Levels"** and **"Access Lists"**.
Basically. there are two ways in which a User can be authorized to access or act upon BFF Models and/or their contents (children):

1. The User is granted a sufficient **"Access Level"** in the Model's **"Access List"**;
2. The User **owns** the Model (i.e. Model's `ownerId` is the User's ID).

> [!NOTE] If a Model is governed by an Access List and is also owned by the acting User, then the User has full rights regardless of the Access List.

See the [Models page](models.md) for details about how authorization is implemented for each Model.

### Level

An Access Level is a bitwise sum of 1-bit permission values.

-   Permissions:

    -   `EAccessPermission.READ = 1 << 0`: Permission to read the contents.
    -   `EAccessPermission.WRITE = 1 << 1`: Permission to write the contents.
    -   `EAccessPermission.DELETE = 1 << 2`: Permission to delete the contents.
    -   `EAccessPermission.MANAGE = 1 << 6`: Permission to alter the Access List and settings.

-   Levels:
    -   `EAccessLevel.READER`: READ;
    -   `EAccessLevel.WRITER`: READ + WRITE;
    -   `EAccessLevel.EDITOR`: READ + WRITE + DELETE;
    -   `EAccessLevel.MANAGER`: READ + WRITE + DELETE + MANAGE;

> [!TIP] Access Levels are scalar values and therefore intrinsically comparable: `MANAGER > EDITOR > WRITER > READER`.

### List

An **"Access List"** is a set of rules that assign **"Access Levels"** to appropriate **"Actors"** (Users or Teams). It is stored in the `access` property of the target Model and has the following structure:

```ts
{
    // Model...

    access: {
        actorId: string,
        level: EAccessLevel[]
    }[];
}
```

### Actors

An `actorId` can be the ID of:

-   a **User**;
-   a **Team**: authorises all the User in the Team;
-   an **Organization**: authorises all the Users in the Organization.

### Example

Here's a more complete example of an Drive's Access List:

```ts
{
    _id: "drive-12345678", // The target Model is a Drive.

    access: [
        // Allow all Users from Organization 'organization-12345678' to read from this Drive.
        { actorId: 'organization-12345678', level: EAccessLevel.WRITER },

        // Allow all Users from Team 'team-12345678' to read from / write to the Drive and also edit the Drive's Access List.
        { actorId: 'team-12345678', level: EAccessLevel.MANAGER },

        // Allow User 'user-12345678' to read from / write to the Drive and also edit the Drive's Access List.
        { actorId: 'user-12345678', level: EAccessLevel.MANAGER },

        // Allow any User to read this Drive (allow public downloads).
        { actorId: 'user-all', level: EAccessLevel.READER },
    ]

    // ...
}
```

## Deletion

In BFF all Models are **soft-deleted** by setting the internal `deletedAt` field and, after a configurable amount of time passes, a dedicated background "garbage-collection" job will **hard-delete** the data resources and DB-records.

This strategy enables two things:

-   **safety**: this enables the "Trash can" functionality, allowing for data-recovery for some time after deletion.
-   **performance**: releasing resources can be time-consuming and it's preferable to defer this work to the backgrund.
