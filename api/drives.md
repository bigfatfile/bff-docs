# Drives API

## Methods

### drives.create

Create a new Drive as a child of the current Account.

-   Parameters:
    -   `name` (String): Drive name.
    -   `description` (String, optional): Drive description.
    -   `warehouseId` (String, optional): Custom Warehouse ID.
-   Returns:
    -   `Promise<string>`: New Drive ID.

### drives.update

Update Drive details.

-   Requires:
    -   MANAGE AccessPermission on the target Drives.
-   Parameters:
    -   `driveId` (String): Drive ID string.
    -   `name` (String): New Drive name.
    -   `description` (String, optional): New Drive description.
-   Returns:
    -   `Promise<void>`.

### drives.remove

Delete a Drive object.

-   Requires:
    -   MANAGE AccessPermission on the target Drives.
-   Parameters:
    -   `driveId` (String): Drive ID string.
-   Returns:
    -   `Promise<void>`.

## Publications

### drives.all

Publishes all Drives accessible to the current user (any level).

-   Requires:
    -   READ AccessPermission on the target Drives.
-   Subscribes:
    -   Drive.
