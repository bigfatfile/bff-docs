# API

The BigFatFile API is a low-level API implemented over the [DDP protocol](https://github.com/meteor/meteor/blob/devel/packages/ddp/DDP.md) which is designed to work over connected WebSockets (DDP was created part of the [MeteorJS](https://www.meteor.com/) framework).

DDP enables pushing of data from the server to the client, thus allowing the BFF Service to update connected clients of changes comming in concurrently from separate users. This results in better user experience and collaboration features.

> This API works over WebSockets and therefore it's designed to keep an active connection to the server. Although slightly more complex, this is preferable for BFF client applications, which usually implement reactive functionalities which benefit from the event-driven design of the DDP protocol.

## How to use

To access the API you should (in order):

1. Open a WebSocket connection to the BFF service endpoint (`api.bigfatfile.com`);
2. Authenticate (`.login()` method);
3. Call Methods or...
4. Subscribe to Publications and listen for updates.

Below, there are same simple examples of how to achieve this. Please we aware that in real-life scenarios, these interactions will probably be slightly more complex depending on your needs and the [DDP Client](http://www.meteorpedia.com/read/DDP_Clients) used. In the examples below we're using SimpleDDP ([npm package](https://www.npmjs.com/package/simpleddp), [git repo](https://github.com/Gregivy/simpleddp)). Please check the SimpleDDP [documentation](https://gregivy.github.io/simpleddp/simpleDDP.html) for more detailed examples and reference.

## Connecting to the service endpoint

```js
import SimpleDDP from "simpleddp";
import ws from "isomorphic-ws";

const service = new SimpleDDP({ ,
    endpoint: "ws://api.bigfatfile.com/", // ...or your own deployment URI.
    SocketConstructor: WebSocket,
    reconnectInterval: 5000,
});
await service.connect();
```

## Calling API Methods

```js
try {
    await service.call("sign-in", username, password);
    /* Success! */
} catch (error) {
    /* Error! */
}
```

## Subscribing to and retrieving data

```js
let nodeId = null; // Start folder.
let subscription = service.subscribe("list:folder", driveId, nodeId);

await subscription.ready();
const nodes = service.collection("nodes").reactive();

nodes.onChange((nodes) => {
    console.log(nodes);
});
```
