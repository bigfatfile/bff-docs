# API: Groups

## Methods

### groups.create

Create a new Group as a child of the current Account which must be of type ORGANIZATION.

-   Parameters:
    -   `name` (string): Group name.
    -   `description` (optional, string): Group description.
-   Returns: New Group ID.

### groups.update

Update Group details.

-   Requires MANAGE AccessPermission on the target Group.
-   Parameters:
    -   `groupId` (string): Group ID.
    -   `name` (string): New Group name.
    -   `description` (optional, string): New Group description.
-   Returns: void

### groups.remove

Delete a Group object.

-   Requires MANAGE AccessPermission on the target Group.
-   Parameters:
    -   `groupId` (string): Group ID.
-   Returns: void

### groups.addMembers

Add users to this Group using their IDs or email addresses. Users without an account will be invited (enrolled) to first create a BFF account via email.

-   Requires WRITE AccessPermission on the target Group.
-   Parameters:
    -   `groupId` (string): Group ID.
    -   `accountIdsOrEmails` (string[]): Array of users to be added (IDs and/or emails).
-   Returns: void

### groups.suggestMembers

Return a set of suggested new members for the given Group ID, starting from a search query.

-   Requires WRITE AccessPermission on the target Group.
-   Parameters:
    -   `groupId` (string): Group ID.
    -   `query` (string): Search string (minimum 3 characters).
-   Returns: An array of objects of type: `{_id: string, fullName: string, email: string, picture?: string}`.

### groups.removeMembers

Remove an array of users from this Group based on their IDs (NOT! emails).

-   Requires DELETE AccessPermission on the target Group.
-   Parameters:
    -   `groupId` (string): Group ID.
    -   `userIds` (string[]): Array of user IDs to be deleted.
-   Returns: void

## Publications

### groups.all

Retrieve all Groups owned by an Organization which the currently authenticated Account has access to (any level).

-   Requires READ AccessPermission on the target Groups.
-   Subscribes: Group

### groups.parentsOfCurrentAccount

Retrieve all Groups which the current user is a member of.

-   Subscribes: Group

### groups.membersOfGroup

Retrieve all members of the given Group.

-   Requires READ AccessPermission on the target Groups.
-   Parameters:
    -   `groupId` (string): The host Group ID.
-   Subscribes: Account | Group | Organization
