# BigFatFile SDK

-   MIT License
-   Git repo: https://gitlab.com/bigfatfile/bff-sdk

This is a Typescript-based Software Development Kit for building applications that are intended to work within the BigFatFile ecosystem.

> [!NOTE]
> This SDK is used as a starting point (and singl source of truth) all the core BFF services and clients and is meant to be the **single-source-of-truth** for both the internal **data architecture** as well as the **client-interfacing APIs**.

> [!WARNING]
> The SDK as well as this documentation are still under development and did not reach a stable release yet, therefore breaking changes may still occur.

**Install**

```sh
yarn add @bigfatfile/sdk # or... npm i @bigfatfile/sdk
```

**Usage**

```ts
import BFF from '@bigfatfile/sdk`
```
